<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <title>Sign in to GreenRoute</title>

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Core stylesheets -->
    <link href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/css/bootstrap-dark.min.css" rel="stylesheet" type="text/css">
    <link href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/css/pixeladmin-dark.min.css" rel="stylesheet" type="text/css">
    <link href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/css/widgets.min.css" rel="stylesheet" type="text/css">

    <!-- Theme -->
    <link href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/css/themes/mint-dark.min.css" rel="stylesheet" type="text/css">

    <!-- Pace.js -->
    <script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/pace/pace.min.js"></script>

    <!-- Custom styling -->
    <style>
        .page-signin-header {
            box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
        }

        .page-signin-header .btn {
            position: absolute;
            top: 12px;
            right: 15px;
        }

        html[dir="rtl"] .page-signin-header .btn {
            right: auto;
            left: 15px;
        }

        .page-signin-container {
            width: auto;
            margin: 30px 10px;
        }

        .page-signin-container form {
            border: 0;
            box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
        }

        @media (min-width: 544px) {
            .page-signin-container {
                width: 350px;
                margin: 60px auto;
            }
        }

        .page-signin-social-btn {
            width: 40px;
            padding: 0;
            line-height: 40px;
            text-align: center;
            border: none !important;
        }

        #page-signin-forgot-form { display: none; }
    </style>
    <!-- / Custom styling -->
</head>
<body >
<div class="page-signin-header p-a-2 text-sm-center bg-white">
    <a class="px-demo-brand px-demo-brand-lg text-default" href="index.html"><span class="px-demo-logo bg-primary m-t-0"><span class="px-demo-logo-1"></span><span class="px-demo-logo-2"></span><span class="px-demo-logo-3"></span><span class="px-demo-logo-4"></span><span class="px-demo-logo-5"></span><span class="px-demo-logo-6"></span><span class="px-demo-logo-7"></span><span class="px-demo-logo-8"></span><span class="px-demo-logo-9"></span></span>GreenRoute</a>
</div>
<!-- Sign In form -->

<div class="page-signin-container" id="page-signin-form">
    <h2 class="m-t-0 m-b-4 text-xs-center font-weight-semibold font-size-20">Sign In to your Account</h2>

    <form action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/user/login" method="POST" class="panel p-a-4">
        <div class="form-group">
            <fieldset class=" form-group form-group-lg">
                <input type="text" name="email" class="form-control form-control-error" placeholder="E-mail address" required autofocus>
            </fieldset>
        </div>

        <div class="form-group">
            <fieldset class=" form-group form-group-lg">
                <input type="password" name="password" class="form-control form-control-error" placeholder="Password" required>
            </fieldset>
        </div>

        <div class="clearfix">
            <label class="custom-control custom-checkbox pull-xs-left">
                <input type="checkbox" name="remember" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                Remember me
            </label>
            <a href="#" class="font-size-12 text-muted pull-xs-right" id="page-signin-forgot-link">Forgot your password?</a>
        </div>

        <button type="submit" class="btn btn-block btn-lg btn-primary m-t-3">Sign In</button>
        <br/>
        <div style="text-align: center;">or</div>
        <button id="page-signin-register" class="btn btn-block btn-lg btn-primary m-t-3">Register</button>
    </form>
</div>

<!-- / Sign In form -->

<!-- Reset form -->

<div class="page-signin-container" id="page-signin-forgot-form">
    <h2 class="m-t-0 m-b-4 text-xs-center font-weight-semibold font-size-20">Password reset</h2>

    <form action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/resetpass" method="POST" class="panel p-a-4">
        <fieldset class="form-group form-group-lg">
            <input type="email" name="email" class="form-control" placeholder="Your Email">
        </fieldset>

        <button type="submit" class="btn btn-block btn-lg btn-primary m-t-3">Send password reset link</button>
        <div class="m-t-2 text-muted">
            <a href="#" id="page-signin-forgot-back">&larr; Back</a>
        </div>
    </form>
</div>

<!-- / Reset form -->

<!-- Register form -->

<div class="page-signin-container" id="page-signin-register-form" style="display: none">
    <h2 class="m-t-0 m-b-4 text-xs-center font-weight-semibold font-size-20">Register</h2>

    <form action="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/user/register" method="POST" class="panel p-a-4">
        <fieldset class="form-group form-group-lg">
            <input type="email" name="email" class="form-control" placeholder="Your Email">
        </fieldset>

        <div class="form-group">
            <fieldset class=" form-group form-group-lg">
                <input type="password" name="password" class="form-control form-control-error" placeholder="Password" required>
            </fieldset>
        </div>

        <button type="submit" class="btn btn-block btn-lg btn-primary m-t-3">Register</button>
        <div class="m-t-2 text-muted">
            <a href="#" id="page-signin-register-back">&larr; Back</a>
        </div>
    </form>
</div>

<!-- / Register form -->
<!-- ==============================================================================
|
|  SCRIPTS
|
=============================================================================== -->

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Core scripts -->
<script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/js/bootstrap.min.js"></script>
<script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/js/pixeladmin.min.js"></script>

<!-- Your scripts -->
<script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/GreenRoute/assets/js/app.min.js"></script>

<script>
    // -------------------------------------------------------------------------
    // Initialize page components

    $(function() {

        $('#page-signin-forgot-link').on('click', function(e) {
            e.preventDefault();

            $('#page-signin-form').css({ display: 'none' });
            $('#page-signin-forgot-form').css({ display: 'block' });

            $(window).trigger('resize');
        });

        $('#page-signin-forgot-back').on('click', function(e) {
            e.preventDefault();

            $('#page-signin-form').css({ display: 'block' });
            $('#page-signin-forgot-form').css({ display: 'none' });

            $(window).trigger('resize');
        });

        $('#page-signin-register').on('click', function(e) {
            e.preventDefault();

            $('#page-signin-register-form').css({ display: 'block' });
            $('#page-signin-form').css({ display: 'none' });

            $(window).trigger('resize');
        });

        $('#page-signin-register-back').on('click', function(e) {
            e.preventDefault();

            $('#page-signin-form').css({ display: 'block' });
            $('#page-signin-register-form').css({ display: 'none' });

            $(window).trigger('resize');
        });

        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
</body>
</html>