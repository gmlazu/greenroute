<?php

class File {
    private $OBJECT_NAME_PHOTO = 'cardphoto';
    private $OBJECT_NAME_DATA = 'carddata';
    private $MAX_FILE_SIZE = 10485760; //10Mb
    private $ORIGINAL_FILE_NAME;
    private $TEMP_FILE_HASH;

    private $sess;

    public function downloadFile($fileName) {
        global $LOGGER;

        if($this->isValidSession() === FALSE) {
            return;
        }

        $data = explode('.', $fileName);
        if(count($data) > 2) {
            $LOGGER->logToClient('810', 'Da fak is dis filename?');
        }

        $LOGGER->logToClient('810', $fileName);
        $LOGGER->logToClient('810', $data[0]);

        global $DATABASE;
        $result = $DATABASE->GetRows('cards', 'cardName = \'' . $data[0] . '\' AND cardOwner = \'' . $this->sess['USERID'] . '\'');
        if ($result->num_rows > 0) {
            // Download file here
            $row = $result->fetch_assoc();
            $pidgeon = $row['cardPath'] . '/' . $row['cardName'] . '.' . $data[1];
            //$LOGGER->logToClient('810', $pidgeon);
            $this->attachHeaders($pidgeon);
            //$this->Download($pidgeon);
        } else {
            $LOGGER->logToClient('810', 'No card found in DB with this name');
        }
    }

    function attachHeaders($file_name) {
        header("X-Sendfile: $file_name");
        header("Content-type: application/octet-stream");
        header('Content-Disposition: attachment; filename="' . basename($file_name) . '"');
    }

    public function processFile() { //TODO log something to the client to get the result
        global $LOGGER;

        if($this->isValidSession() === FALSE) {
            return;
        }

        try {
            $folder = $this->getStorageFolder();
            $this->ORIGINAL_FILE_NAME = $this->getOriginalFileName();
            $this->getFile($folder, $this->OBJECT_NAME_PHOTO);
            $this->getFile($folder, $this->OBJECT_NAME_DATA);

            $this->addToDB(
                $this->ORIGINAL_FILE_NAME,
                $folder,
                $this->TEMP_FILE_HASH,
                $this->sess['USERID']);

            $LOGGER->logToServer('800', $this->ORIGINAL_FILE_NAME);
        } catch (Exception $ex) {
            $LOGGER->logToClient('801', $ex->getMessage());
        }
    }

    /*
     * Deletes the specified card name for the currently logged in user
     */
    public function deleteCard($cardName) {
        global $LOGGER;

        if($this->isValidSession() === FALSE) {
            return;
        }

        global $DATABASE;
        $result = $DATABASE->GetRows('cards', 'cardName = \'' . $cardName . '\' AND cardOwner = \'' . $this->sess['USERID'] . '\'');
        if ($result->num_rows > 0) {
            // Delete file here
            $row = $result->fetch_assoc();
            $this->deleteFiles($LOGGER, $row['cardPath'], $row['cardName']);
            $DATABASE->DeleteRow('cards', 'cardName = \'' . $row['cardName'] . '\' AND cardOwner = \'' . $this->sess['USERID'] . '\'');
            //$LOGGER->logToClient('800', $row['cardPath'] . "\t" . $row['cardName']);

        } else {
            $LOGGER->logToClient('811', 'No card to delete');
        }
    }

    /*
     * Deletes all user cards for specified $uid
     */
    public function deleteAllUserCards($uid) {
        global $LOGGER;
        global $DATABASE;
        $result = $DATABASE->GetRows('cards', 'cardOwner = \'' . $uid . '\'');
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                //Delete its cards here
                $this->deleteFiles($LOGGER, $row['cardPath'], $row['cardName']);
            }
            $DATABASE->DeleteRow('cards', 'cardOwner=\'' . $uid . '\''); //Finally delete everything from the database
        } else {
            //User has no cards echo "0 results";
        }
    }

    /*
     * Delete 'old' data about a card and gets the new one
     */
    public function updateCardDetails() {
        global $LOGGER;

        if($this->isValidSession() === FALSE) {
            return;
        }

        $this->ORIGINAL_FILE_NAME = $this->getOriginalFileName();

        global $DATABASE;
        $result = $DATABASE->GetRows('cards', 'cardName = \'' . $this->ORIGINAL_FILE_NAME . '\' AND cardOwner = \'' . $this->sess['USERID'] . '\'');
        if ($result->num_rows > 0) {
            // Delete old file here
            $row = $result->fetch_assoc();

            $LOGGER->logToServer('100', 'Updating ' . $row['cardName'] . ' whith path ' . $row['cardPath']);
            $this->getNewDataFile($LOGGER, $row['cardPath']); //remember that this also overwrites the file
        } else {
            $LOGGER->logToClient('902', '');
        }
    }

    private function getNewDataFile($LOGGER, $folder) {
        try {
            $this->getFile($folder, $this->OBJECT_NAME_DATA);
        } catch (Exception $ex) {
            $LOGGER->logToClient('901', $ex->getMessage());
        }
    }

    public function getCards() {
        global $LOGGER;

        if($this->isValidSession() === FALSE) {
            return;
        }

        global $DATABASE;
        $result = $DATABASE->GetRows('cards', 'cardOwner = \'' . $this->sess['USERID'] . '\'');
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $LOGGER->logCardToClient($row['cardPath'], $row['cardName']);
            }
        } else {
            $LOGGER->logToBoth('100', 'Your cloud cardholder is empty. Try uploading some cards.');
        }
    }

    function isValidSession() {
        include 'session.php';
        global $LOGGER;

        $this->sess = $SESSION->getSessionValues();

        if($this->sess['VALID'] === FALSE) {
            $LOGGER->logToClient('805', '');
            return false;
        }
        return true;
    }

    function deleteFiles($LOGGER, $filePath, $fileName) {
        $ok1 = $this->deleteFile($filePath, $fileName . '.jpg');
        $ok2 = $this->deleteFile($filePath, $fileName . '.data');
        if($ok1 && $ok2) {
            $LOGGER->logToServer('810', 'Deleted ' . $fileName . ' result was 1');
            $LOGGER->logToClient('810', '');
        } else {
            $LOGGER->logToBoth('811', 'Deleted ' . $fileName . ' result was 0');
        }
    }

    function deleteFile($filePath, $fileName) {
        return unlink($filePath . '/' . $fileName);
    }

    function getStorageFolder() {
        global $LOGGER;
        global $UTILS;

        if($this->sess['VALID'] === FALSE) {
            throw new RuntimeException('ERROR: You are not logged in. Please log in and try again');
        }

        if($this->isUserAllowedToStore() === FALSE) {
            throw new RuntimeException('You have reached your maximum number of cards you are allowed to store with a free account. Would you like to upgrade to PRO?');
        }

        $this->TEMP_FILE_HASH = $this->getTempFileHash();

        $folder = $UTILS->createFolders(3);
        if($folder === false) {
            throw new RuntimeException('Can\'t get new folder path.');
        }
        return $folder;
    }

    function isUserAllowedToStore() {
        global $DATABASE;

        $users = $DATABASE->GetRows('users', 'userId = \'' . $this->sess['USERID'] . '\'');
        if($users->num_rows > 0) {
            $user = $users->fetch_assoc();
            if($user['userType'] == 1) {
                return true;
            } else {
                $result = $DATABASE->GetRows('cards', 'cardOwner = \'' . $this->sess['USERID'] . '\'');
                if($result->num_rows > 4) { //Free users can only have 5 cards
                    return false;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    function getTempFileHash() {
        $hash = md5_file($_FILES[$this->OBJECT_NAME_PHOTO]['tmp_name']);

        if(!$this->checkForFileHash($hash)) {
            throw new RuntimeException('Found similar card in database. Aborting file save.');
        }

        return $hash;
    }

    function getOriginalFileName() { //TODO remove file extention
        $jsonarray = json_decode(file_get_contents($_FILES[$this->OBJECT_NAME_DATA]['tmp_name']), true);
        return substr($jsonarray['photo_file'], 0, -4);
    }

    function getFile($folder, $objname) {
        $this->checkForCorruptFile($objname);
        $ext = $this->getMIMEifValid($objname);
        $this->saveFile($folder, $objname, $ext);
    }

    function checkForCorruptFile($objname) {
        // Undefined | Multiple Files | $_FILES Corruption Attack
        // If this request falls under any of them, treat it invalid.
        if (
            !isset($_FILES[$objname]['error']) ||
            is_array($_FILES[$objname]['error'])
        ) {
            throw new RuntimeException('Invalid parameters.');
        }

        // Check $_FILES['upfile']['error'] value.
        switch ($_FILES[$objname]['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new RuntimeException('Exceeded filesize limit.');
            default:
                throw new RuntimeException('Unknown errors.');
        }

        // You should also check filesize here.
        if ($_FILES[$objname]['size'] > $this->MAX_FILE_SIZE) {
            throw new RuntimeException('Exceeded filesize limit.');
        }
    }

    function getMIMEifValid($objname) { //Also returns file extension
        // DO NOT TRUST $_FILES[$objname]['mime'] VALUE !!
        // Check MIME Type by yourself.
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        if (false === $ext = array_search(
            $finfo->file($_FILES[$objname]['tmp_name']),
            array(
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif',
                'data' => 'text/plain',
            ),
            true
        )) {
            throw new RuntimeException('Invalid file format.');
        }
        return $ext;
    }

    function saveFile($path, $objname, $ext) {
        // You should name it uniquely.
        // DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
        // On this example, obtain safe unique name from its binary data.
        global $LOGGER;

        $filename = $path . '/' . $this->ORIGINAL_FILE_NAME . '.' . $ext;
        $ok = move_uploaded_file(
            $_FILES[$objname]['tmp_name'],
            $filename);

        if (!$ok) {
            throw new RuntimeException('Failed to move uploaded file.');
        }

        if ($ext === 'jpg' || $ext === 'png' || $ext === 'jpeg') { //optimize the file if it's a photo
            $this->optimizePhoto($path, $ext);
        }
    }

    function optimizePhoto($path, $ext) {
        global $LOGGER;
        global $UTILS;

        $filename = $path . '/' . $this->ORIGINAL_FILE_NAME . '.' . $ext;

        $filename = escapeshellcmd($filename);

        if ($ext === 'jpg' || $ext === 'png' || $ext === 'jpeg') { //optimize the file if it's a photo
            $cmd =
                "php /var/www/html/api/UTILITARIES/tiny.php '" .
                $filename .
                "'" .
                " > /dev/null &";

            $LOGGER->logToServer('100', 'cmd is ' . $cmd);
            exec($cmd);
        }
    }

    function checkForFileHash($hash) {
        global $DATABASE;

        $rows = $DATABASE->GetRows('cards', 'fileHash = \'' . $hash . '\' AND cardOwner = \'' . $this->sess['USERID'] . '\'');
        return $rows->num_rows == 0;
    }

    function addToDB($name, $path, $hash, $uid) {
        global $DATABASE;

        $data = [
                'cardName' => $name,
                'cardPath' => $path,
                'fileHash' => $hash,
                'cardOwner' => $uid
                ];
        $DATABASE->InsertRow('cards', $data);
    }
}

$FILE = new File();
