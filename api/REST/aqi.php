<?php

class AQI {
    function getAroundLocation($lat, $lng, $radius) {
        global $DATABASE;
        $found = $DATABASE->GetRowsByRadius($lat, $lng, $radius);
        $data = [];
        while($row = $found->fetch_assoc()) {
            array_push($data, $row);
        }
        echo json_encode($data);
    }
}

$AQI = new AQI();

