<?php

class Utils
{
    public $YEARS_TO_SECONDS = 31536000;
    function getSQLTime($time)
    {
            return date('Y-m-d H-i-s', $time);
    }
    function return500()
    {
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    }
    function getHTTPJson($array)
    {
        $preput = json_encode($array,  JSON_UNESCAPED_SLASHES);
        return $preput;
    }
	
    function validateEmail($email) {
        if(empty($email)) {
            return false;
        } else {
            return filter_var($email, FILTER_VALIDATE_EMAIL);
        }
    }
    
    function createFolders($depth) {
        global $LOGGER;
        
        $hash = $this->generateToken(10);
        $path = $this->getFolderPath($hash, $depth);
                
        if (!file_exists($path)) {
            $LOGGER->logToClient('802', $path);
            $result = mkdir($path, 0777, true);
            if($result) {
                return $path;
            } else {
                $LOGGER->logToClient('803', $path);
                return false;
            }
        }
        return $path;
    }
    
    function getFolderPath($hash, $depth) {
        $path = '/var/www/storage/';
        for($index = 0; $index < $depth; $index++) {
            $path .= $hash[$index] . '/';
        }
        $path .= substr($hash, $depth);
        return $path;
    }
    
    public function generateToken($tokenLength = 26) {
        $dummy_token = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $dummy_token_length = 61;
        $token = '';
        
        for ($i = 0; $i < $tokenLength; $i++) {
            $token .= $dummy_token[rand(0, $dummy_token_length)];
        }
        return $token;
    }
	
    function validatePassword($password) //TODO: SET PASSWORD REQUIREMENTS
    {
        return (!empty($password));
    }
}
$UTILS = new Utils();