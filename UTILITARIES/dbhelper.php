<?php
abstract class Tables {
    const Sessions = 'sessions';
    const Users = 'users';
    const Cards = 'cards';
}

class DBHelper{
    private $servername = "defcon33.ddns.net";
    //private $servername = 'localhost';
    private $username = 'glazura';
    private $password = 'binbash';
    private $dbname = 'greenRoute';
    
    private $userstable = 'users';
    private $sessionstable = 'sessions';
    
    public $conn;
    
    public $secondsInYear = 31536000;
	
    public $initialized = false;
    private $obj;

    function initialize() {
        global $LOGGER;
        // Create connection
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);

        // Check connection
        if ($this->conn->connect_error) {
            $LOGGER->logToServer('700', 'Could not initialize database connection');
        } else {
            $this->initialized = true;
        }
    }
	
    function SQLQuery($toquery)
    {
        if(!$this->initialized) {
            $this->initialize();
        }
        global $LOGGER;
        $returned = $this->conn->query($toquery);
        if(!$returned){
            $LOGGER->logToServer('701', $toquery);
        }
        return $returned;
    }
    function InsertRow($table, $data)
    {
        //TODO IF WANTED: ADD VERIFICATION FOR VALID COLUMN NAMES (gets rejected anyway)
        $FP = 'INSERT INTO ' . $table . ' (';
        $SP = 'VALUES (';
        foreach($data as $key=>$value)
        {
            $FP .= $key . ',';
            $SP .= ( '\'' . $value .'\',' );
        }
        $FP = substr($FP, 0, -1);
        $SP = substr($SP, 0, -1);
        $FP .= ') ';
        $SP .= ');';
        return $this->SQLQuery($FP . $SP);
    }
    function DeleteRow($table, $evaluation)
    {
        $pidgeon = 'DELETE FROM ' . $table . ' WHERE ' . $evaluation . ';';
        return $this->SQLQuery($pidgeon);
    }
    function UpdateRow($table, $data, $evaluation)
    {
        while($evaluation != trim($evaluation)) {
            $evaluation = trim($evaluation);
        }

        if($evaluation == '') {
            return false;
        }
        
        $pidgeon = 'UPDATE ' . $table . ' SET ';
        foreach($data as $key=>$value) {
            $pidgeon .= ($key . '=\'' . $value . '\',');
        }
        $pidgeon = substr($pidgeon, 0, -1);
        $pidgeon .= ' WHERE '. $evaluation . ';';
        return $this->SQLQuery($pidgeon);
    }
    function GetRows($table, $evaluation)
    {
        $pidgeon = 'SELECT * FROM '. $table.' WHERE '. $evaluation . ';';
        return $this->SQLQuery($pidgeon);
    }
    function GetRowsForColumn($table, $column, $evaluation) {
        $pidgeon = 'SELECT '. $column .' FROM '. $table.' WHERE '. $evaluation . ';';
        return $this->SQLQuery($pidgeon);
    }
}

$DATABASE = new DBHelper();