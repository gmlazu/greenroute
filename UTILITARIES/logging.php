<?php

class Log
{
    private $logsFile = '/var/www/html/api/main.log';
    private $messageBuilder = [];
    private $cardsBuilder = [];
    private $userType;
    private $outputJSON = false;
    private $messageCodes =
    [
        '100' => 'INFO: ', //Generic things
        '101' => 'ERROR: ',
        '102' => 'DEBUG: ',

        '500' => 'INFO: User registered successfully',
        '501' => 'ERROR: Invalid email or password',
        '502' => 'ERROR: Email address already in use',
        '503' => 'ERROR: Error adding the user to the database',

        '600' => 'INFO: User logged in successfully',
        '601' => 'ERROR: Failed to login the user',
        '602' => 'ERROR: User already logged in',
        '603' => 'INFO: User logged out successfully',
        '604' => 'ERROR: Could not log out',
        '605' => 'INFO: User deleted successfully',
        '606' => 'ERROR: Could not delete user',
        '607' => 'INFO: You are now a pro user',
        '608' => 'ERROR: Failed upgrading to pro',
        '609' => 'INFO: We\'ve sent you an email containing the instructions needed to reset your password',
        '610' => 'ERROR: There is no account associated to this email address',
        '611' => 'ERROR: Failed sending reset password email',
        '612' => 'ERROR: Empty passwords are not allowed',
        '613' => 'ERROR: The token is no longer valid. Please try again',
        '614' => 'INFO: Successfully changed password. You can now use the app!',
        '615' => 'ERROR: You need a password token!',

        '700' => 'ERROR: Could not connect to database',
        '701' => 'ERROR: Failed query to database',

        '800' => 'INFO: Upload succeeded',
        '801' => 'ERROR: Upload failed',
        '802' => 'DEBUG: Creating new folder',
        '803' => 'ERROR: Failed creating new folder',
        '804' => 'INFO: File description',
        '805' => 'ERROR: You are not logged in. Please log in and try again',

        '810' => 'INFO: Successfully deleted business card',
        '811' => 'ERROR: Could not delete file',

        '900' => 'DEBUG: Optimizing photo with path',
        '901' => 'ERROR: Could not optimize this photo because of an error',
        '902' => 'INFO: Successfully optimized this photo',
    ];

    public function logCardToClient($path, $name) {
        array_push($this->cardsBuilder, ['path' => $path, 'name' => $name]);
    }

    public function logToClient($messagecode, $message)
    {
        array_push($this->messageBuilder, ['code' => $messagecode, 'event' => $this->messageCodes[$messagecode], 'message' => $message]);
    }

    public function logToServer($messagecode, $message)
    {
        file_put_contents($this->logsFile, $messagecode . ' ' . $this->messageCodes[$messagecode] .PHP_EOL. "\t-" . $message . PHP_EOL, FILE_APPEND);
    }

    public function logToBoth($messagecode, $message) {
        $this->logToClient($messagecode, $message);
        $this->logToServer($messagecode, $message);
    }

    public function logUserType($userType) {
        $this->userType = $userType;
    }

    public function setOutputJSON($val) {
        $this->outputJSON = $val;
    }

    function __destruct()
    {
        if ($this->outputJSON) {
            echo json_encode([
                'user_type' => $this->userType,
                'json_messages' => $this->messageBuilder,
                'cards' => $this->cardsBuilder], JSON_UNESCAPED_SLASHES);
        }
    }
}
$LOGGER = new Log();
?>
