/* local pollution data fetcher */
function fetchPollutionData() {
    $.getJSON('http://defcon33.ddns.net/GreenRoute/api/poldata.json', function (data) {
        var config = {
            animate: 2000,
            scaleColor: false,
            lineWidth: 4,
            lineCap: 'square',
            size: 90,
            trackColor: 'rgba(0 , 0, 0, .09)'
        }
        $.each(data, function (k, v) {
            v = parseFloat(v);
            var perc;
            var color;
            if (k == "NO2") {
                if (v <= 0.053) {
                    perc = 30;
                    color = "#00E400";
                } else if (v <= 0.6) {
                    perc = 60;
                    color = "#FFFF00";
                } else if (v <= 1.2) {
                    perc = 80;
                    color = "#FF7E00";
                } else {
                    perc = 100;
                    color = "#FF0000";
                }
            } else {
                if (v <= 350) {
                    perc = 30;
                    color = "#00E400";
                } else if (v <= 1000) {
                    perc = 40;
                    color = "#FFFF00";
                } else if (v <= 2000) {
                    perc = 60;
                    color = "#FF7E00";
                } else if (v <= 5000) {
                    perc = 80;
                    color = "#FF0000";
                } else {
                    perc = 100;
                    color = "#7E0023";
                }
            }
            if (k == "NO2") v *= 1000;
            var unit = (k === "NO2") ? "b" : "m";
            $('#local-pollution-' + k)
                .attr('data-percent', perc)
                .attr('data-max-value', 100)
                .easyPieChart($.extend({barColor: color}, config))
                .find('> span')
                .text(v.toFixed(2) + "pp" + unit);
        });
    });
}
/* location pickers */
$("#from-button").on('click', function () {
    google.maps.event.addListener(map, 'click', function(event) {
        $("#from-form").attr("value", event.latLng);
        addMarker(event.latLng, map, "A");
        google.maps.event.clearListeners(map, 'click');
    });
});
$("#to-button").on('click', function () {
    google.maps.event.addListener(map, 'click', function(event) {
        $("#to-form").attr("value", event.latLng);
        addMarker(event.latLng, map, "B");
        google.maps.event.clearListeners(map, 'click');
    });
});
/* get surrounding data */
function getSurroundingData(lat, lng, radius) {
    $.getJSON("http://defcon33.ddns.net/GreenRoute/api/aqi?lat="+lat+"&lng="+lng+"&radius="+radius, function(data) {
        var avg = 0;
        var cnt = 0;
        for(i = 0; i < data.length; i++) {
            avg += parseInt(data[i]["aqi"]);
            cnt++;
        }
        var aqi = Math.floor(avg/cnt);
        $("#aqi").html(aqi);
        if (aqi <= 50) {
            $("#aqi-parent").css("background-color", "#00E400");
        } else if (aqi <= 100) {
            $("#aqi-parent").css("background-color", "#FFFF00");
        } else if (aqi <= 150) {
            $("#aqi-parent").css("background-color", "#FF7E00");
        } else if (aqi <= 200) {
            $("#aqi-parent").css("background-color", "#FF0000");
        } else if (aqi <= 300) {
            $("#aqi-parent").css("background-color", "#8f3f97");
        } else if (aqi <= 500) {
            $("#aqi-parent").css("background-color", "#7E0023");
        }
    });
}
/* get heatmap data */
function getHeatMapData(lat, lng, radius) {
    $.getJSON("http://defcon33.ddns.net/GreenRoute/api/aqi?lat="+lat+"&lng="+lng+"&radius="+radius, function(data) {
        var heatmap = [];
        for(i = 0; i < data.length; i++) {
            heatmap.push({location: new google.maps.LatLng(data[i]['lat'], data[i]['lng']), weight: parseInt(data[i]['aqi'])*10});
        }
        var heatmap = new google.maps.visualization.HeatmapLayer({
            data: heatmap
        });
        //heatmap.set('dissipating', true);
        //heatmap.set('radius', 100);
        //heatmap.set('opacity', 0.5)
        //heatmap.setMap(map);
    });
}
/* get heatmap data */
function getMarkers(lat, lng, radius) {
    $.getJSON("http://defcon33.ddns.net/GreenRoute/api/aqi?lat="+lat+"&lng="+lng+"&radius="+radius, function(data) {
        var markers = [];
        var infowindows = [];
        for(i = 0; i < data.length; i++) {
            var contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h1 id="firstHeading" class="firstHeading" style="color: black">'+data[i]["nume"]+'</h1>'+
                '<div id="bodyContent">'+
                '<p style="color: black"><b>Air Quality Index: '+ data[i]['aqi'] +'</b><br/>' +
                'Particulate Matter: Hi: ' +data[i]['pm25'][2] + ', Lo: '+data[i]['pm25'][1]+', Current: '+data[i]['pm25'][0]+'<br/>' +
                'Ozone: Hi: ' +data[i]['o3'][2] + ', Lo: '+data[i]['o3'][1]+', Current: '+data[i]['o3'][0]+'<br/>' +
                'Nitrogen Dioxide: Hi: ' +data[i]['no2'][2] + ', Lo: '+data[i]['no2'][1]+', Current: '+data[i]['no2'][0]+'<br/>' +
                'Sulfur Dioxide: Hi: ' +data[i]['so2'][2] + ', Lo: '+data[i]['so2'][1]+', Current: '+data[i]['so2'][0]+'<br/>' +
                'Temperature: Hi: ' +data[i]['t'][2] + ', Lo: '+data[i]['t'][1]+', Current: '+data[i]['t'][0]+'<br/>' +
                'Pressure: Hi: ' +data[i]['p'][2] + ', Lo: '+data[i]['p'][1]+', Current: '+data[i]['p'][0]+ '<br/>' +
                'Humidity: Hi: ' +data[i]['h'][2] + ', Lo: '+data[i]['h'][1]+', Current: '+data[i]['h'][0]+'<br/>' +
                'Wind: Hi: ' +data[i]['w'][2] + ', Lo: '+data[i]['w'][1]+', Current: '+data[i]['w'][0]+'<br/>' +
                '</p>'+
                '</div>'+
                '</div>';

            infowindows[i] = new google.maps.InfoWindow({
                content: contentString
            });

            markers[i] = new google.maps.Marker({
                position: new google.maps.LatLng(data[i]['lat'], data[i]['lng']),
                map: map,
                title: data[i]['nume']
            });
            google.maps.event.addListener(markers[i], 'click', function(innerKey) {
                return function() {
                    infowindows[innerKey].open(map, markers[innerKey]);
                }
            }(i));
        }
    });
}